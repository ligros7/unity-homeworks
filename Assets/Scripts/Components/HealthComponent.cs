using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace Components
{
    public sealed class HealthComponent : MonoBehaviour, IHealthComponent
    {
        [SerializeField] 
        private int health;
        
        public int Health { get => health; }
        
        public void IncreaseHealth(int value)
        {
            this.health += value;
        }

        public void DecreaseHealth(int value)
        {
            this.health = Math.Max(this.health - value, 0);
        }
    }
}