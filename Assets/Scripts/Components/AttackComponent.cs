using UnityEngine;

namespace Components
{
    public sealed class AttackComponent : MonoBehaviour, IAttackComponent
    {
        [SerializeField] 
        private int attackPower;
        
        public int AttackPower { get => attackPower; }
        
        public void Attack()
        {
            Debug.Log($"Attack someone for {this.attackPower.ToString()} hit points!");
        }
    }
}