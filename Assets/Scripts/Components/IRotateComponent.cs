using UnityEngine;

namespace Components
{
    public interface IRotateComponent
    {
        void Rotate(Vector3 angle);

        Vector3 GetRotation();
    }
}