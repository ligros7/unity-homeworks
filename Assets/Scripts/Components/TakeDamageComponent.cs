using UnityEngine;

namespace Components
{
    public sealed class TakeDamageComponent : MonoBehaviour, ITakeDamageComponent
    {
        [SerializeField] 
        private HealthComponent healthComponent;
        
        public void TakeDamage(int damage)
        {
            healthComponent.DecreaseHealth(damage);
        }
    }
}