using UnityEngine;

namespace Components
{
    public sealed class RotateComponent : MonoBehaviour, IRotateComponent
    {
        [SerializeField] 
        private Transform rotateTransform;

        [SerializeField]
        private float speed;

        public void Rotate(Vector3 angle)
        {
            rotateTransform.Rotate(angle * (Time.deltaTime * speed));
        }

        public Vector3 GetRotation()
        {
            return rotateTransform.eulerAngles;
        }
    }
}