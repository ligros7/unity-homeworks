using UnityEngine;

namespace Components
{
    [RequireComponent(typeof(Transform))]
    public sealed class MoveComponent : MonoBehaviour, IMoveComponent
    {
        [SerializeField] 
        private Transform transform;
        [SerializeField] 
        private float speed;

        private IMoveComponent moveComponentImplementation;

        public float Speed { get => speed; }
        
        public void Move(Vector3 direction)
        {
            transform.position += direction * (Time.deltaTime * speed);
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }
    }
}