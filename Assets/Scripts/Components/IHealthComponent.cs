using UnityEditor;

namespace Components
{
    public interface IHealthComponent
    {
        int Health { get; }

        void IncreaseHealth(int value);
        
        void DecreaseHealth(int value);
    }
}