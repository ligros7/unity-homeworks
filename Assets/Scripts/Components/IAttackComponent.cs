namespace Components
{
    public interface IAttackComponent
    {
        int AttackPower { get; }
        void Attack();
    }
}