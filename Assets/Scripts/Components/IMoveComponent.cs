using UnityEngine;

namespace Components
{
    public interface IMoveComponent
    {
        float Speed { get; }
        void Move(Vector3 direction);

        Vector3 GetPosition();
    }
}
