using System;
using Components;
using Entities;
using UnityEngine;

namespace Controllers
{
    public sealed class RotateController : MonoBehaviour
    {
        [SerializeField]
        private MonoEntity player;

        private IRotateComponent rotateComponent;

        private void Start()
        {
            rotateComponent = player.Element<IRotateComponent>();
        }

        private void Update()
        {
            rotateComponent.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0));
            
            if (Input.GetKeyDown("e"))
            {
                Debug.Log($"My rotation is {rotateComponent.GetRotation().ToString()}!");
            }
        }
    }
}