using System;
using Components;
using Entities;
using UnityEngine;

namespace Controllers
{
    public sealed class MoveController : MonoBehaviour
    {
        [SerializeField] 
        private MonoEntity player;

        private IMoveComponent moveComponent;

        private void Start()
        {
            moveComponent = player.Element<IMoveComponent>();
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                this.moveComponent.Move(new Vector3(0, 0, 1.0f));
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                this.moveComponent.Move(new Vector3(0, 0, -1.0f));
            }
            
            if (Input.GetKey(KeyCode.RightArrow))
            {
                this.moveComponent.Move(new Vector3(1.0f, 0, 0f));
            }
            
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                this.moveComponent.Move(new Vector3(-1.0f, 0f, 0f));
            }
            
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log($"Move speed is {moveComponent.Speed.ToString()}!");
            }
            
            if (Input.GetKeyDown("e"))
            {
                Debug.Log($"My position is {moveComponent.GetPosition().ToString()}!");
            }
        }
    }
}