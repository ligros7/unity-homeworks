using Components;
using Entities;
using UnityEngine;

namespace Controllers
{
    public sealed class AttackController : MonoBehaviour
    {
        [SerializeField] 
        private MonoEntity player;

        private IAttackComponent attackComponent;

        private void Start()
        {
            attackComponent = player.Element<IAttackComponent>();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                attackComponent.Attack();
            }
            
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log($"Attack power is {attackComponent.AttackPower.ToString()}!");
            }
        }
    }
}