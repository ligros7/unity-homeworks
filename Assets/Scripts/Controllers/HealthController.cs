using System;
using Components;
using Entities;
using UnityEngine;

namespace Controllers
{
    public sealed class HealthController : MonoBehaviour
    {
        [SerializeField] 
        private MonoEntity player;

        private IHealthComponent healthComponent;

        private void Start()
        {
            healthComponent = player.Element<IHealthComponent>();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log($"I have {healthComponent.Health.ToString()} health points!");
            }
        }
    }
}