using System;
using Components;
using Entities;
using UnityEngine;

namespace Controllers
{
    public sealed class TakeDamageController : MonoBehaviour
    {
        [SerializeField] 
        private MonoEntity player;

        private ITakeDamageComponent takeDamageComponent;

        private void Start()
        {
            takeDamageComponent = player.Element<ITakeDamageComponent>();
        }

        private void Update()
        {
            if (Input.GetKeyDown("q"))
            {
                takeDamageComponent.TakeDamage(10);
            }
        }
    }
}